# AOS 573 Final Project: Becca Hall
This is the folder containing all the materials for Becca Hall's final AOS 573 project. There are four data files present: one TIMPS tracking dataset for the year 2015 and three ERA5 datasets for each of the thermodynamic variables included in this project. There is also an environment file (which is just the same as the AOS 573 environment) and the jupyter notebook containing the code to conduct the analysis. 
This project is focused on observing the trends in thermodynamic variables over the course of a mesoscale convective system's lifecycle. 
